/**
 *@author
 *shsharma
 */
package org.shobhank;

import java.util.List;

public class DependencyDemo {

    public static void main(String[] args) {
        Dependency dep = new Dependency();
        List<Job> jobs = dep.jsonToJob("workflow.json");
        Graph g = new Graph();
        g.nodes = jobs;
        for (int i = 0; i < g.nodes.size(); i++) {
            Job node = g.nodes.get(i);
            node.degree = g.dfsDegree(node);
            g.setToUninstalled();
        }        
        dep.schedule(g);
    }
}
