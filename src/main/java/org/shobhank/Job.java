/**
*@author
*shsharma
*/
package org.shobhank;
import java.util.List;


public class Job {
    String jobName;
    String jobType;
    List<Job> dependsOn;
    boolean installed = false;
    int degree = 0;
    
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getJobType() {
        return jobType;
    }
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }
    public List<Job> getDependsOn() {
        return dependsOn;
    }
    public void setDependsOn(List<Job> dependsOn) {
        this.dependsOn = dependsOn;
    }
}


