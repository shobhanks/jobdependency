/**
*@author
*shsharma
*/
package org.shobhank;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;


public class Graph {
    List<Job> nodes = new LinkedList<Job>();
    
    int dfsDegree(Job node){
        Stack<Job> s = new Stack<Job>();
        node.installed = true;
        s.push(node);
        int height = 1;
        int maxHeightSoFar = 1;
        while(!s.empty()){
            Job v = s.peek();
            Job w = getUnvisitedNode(v);
            if(w==null){
                if(height>maxHeightSoFar)
                    maxHeightSoFar = height;
                height--;
                s.pop();
            }else{
                w.installed = true;
                height++;
                s.push(w);
            }
        }
        return maxHeightSoFar;
    }
    
    private Job getUnvisitedNode(Job v) {
        for(int i=0;i<v.dependsOn.size();i++){
            Job w = v.dependsOn.get(i);
            if(w.installed==false)
                return w;
        }
        return null;
    }
    
    public void setToUninstalled() {
        for(int i=0;i<nodes.size();i++){
            nodes.get(i).installed = false;
        }
        
    }
}


