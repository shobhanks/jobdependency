/**
*@author
*shsharma
*/
package org.shobhank;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Dependency {

    /*
     * Schedule jobs based on their dependencies.
     * It uses DFS for graph to calculate depths
     * which are used to determine schedule
     */  
    public void schedule(Graph g){
        List<Job> jobs = g.nodes;
        int jobCounter = jobs.size();
        for(int stage=1;stage<=jobs.size();stage++){
            if(jobCounter==0)
                break;
            System.out.println("Stage"+stage);
            for(int i=0;i<jobs.size();i++){
                if(jobs.get(i).degree == stage){
                    printJob(jobs.get(i));
                    jobCounter--;
                }
            }
            System.out.println();
            System.out.println();
        }
    }

    /*
     * Print jobs according to format
     * Stage[x]
     * [JobName]:[JobType](depends_on: [JobName] [JobName] ...)
     * [JobName]:[JobType](depends_on: [JobName] ... )
     * ...
     */
    public void printJob(Job job){
        System.out.print(job.getJobName()+":"+job.getJobType());
        if(job.dependsOn.size()==0){
            System.out.println();
            return;
        }
        System.out.print("(depends_on:");
        for(int i=0;i<job.dependsOn.size();i++){
            System.out.print(" "+ job.dependsOn.get(i).jobName);
        }
        System.out.println(" )");
    }
    
    /*
     * Convert workflow.json to Jobs
     */
    public List<Job> jsonToJob(String filePath) {
        List<Job> jobs = new ArrayList<Job>();
        try {
            FileReader reader = new FileReader(filePath);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("tasks");

            Map<String, Job> strJobsMap = new HashMap<String, Job>();
            Map<String, List<String>> jobDependsOnMap = new HashMap<String, List<String>>();

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject inner = (JSONObject) jsonArray.get(i);
                Set<String> keys = inner.keySet();
                Iterator<String> iter1 = keys.iterator();
                Job job = new Job();
                while (iter1.hasNext()) {
                    String key = iter1.next();
                    job.setJobName(key);

                    JSONObject jJobAtt = (JSONObject) inner.get(key);
                    job.setJobType((String) jJobAtt.get("job_type"));
                    strJobsMap.put(key, job);

                    JSONArray jDependsOn = (JSONArray) jJobAtt
                            .get("depends_on");
                    List<String> dependsOnJobs = new ArrayList<String>();
                    if (jDependsOn != null) {
                        for (int j = 0; j < jDependsOn.size(); j++) {
                            dependsOnJobs.add((String) jDependsOn.get(j));
                        }
                    }
                    jobDependsOnMap.put(key, dependsOnJobs);

                }
                jobs.add(job);
            }

            Set<String> dependsOn = jobDependsOnMap.keySet();
            Iterator<String> iter = dependsOn.iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                Job job = strJobsMap.get(key);
                List<String> depList = jobDependsOnMap.get(key);
                List<Job> depListJobObjects = new ArrayList<Job>();
                for (int i = 0; i < depList.size(); i++) {
                    depListJobObjects.add(strJobsMap.get(depList.get(i)));
                }
                job.setDependsOn(depListJobObjects);
                job.degree = depList.size();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jobs;
    }
}


